from flask import Flask
from flask import request
import json
import importlib
import pymongo
import urllib.request
import os
import tempfile
from sklearn.svm import SVC
from sklearn.datasets import load_iris

app = Flask(__name__)

@app.route('/')
def greet():
    result = 'Hello, dear user!'
    return result

@app.route('/echo', methods=['GET'])
def echo():
   result = request.args.get('echostring')
   return result

@app.route('/calc', methods=['GET', 'POST'])
def calc():
    a = 0
    b = 0
    if request.method == 'POST':
        jsondata = request.json
        a = int(jsondata['a'])
        b = int(jsondata['b'])
    else:
        a = int(request.args.get('a'))
        b = int(request.args.get('b'))
    result = json.dumps({'sum': a + b, 'div': a / b})
    return result

@app.route('/fasta', methods=['POST'])
def fasta():
    jsondata = request.json
    source =  jsondata['source']
    with tempfile.TemporaryDirectory() as tmpdirname:
       fullpath = os.path.join(tmpdirname,'proteins.fasta')
       urllib.request.urlretrieve(source, fullpath)
       function =  jsondata['function']
       arg = jsondata['arg']
       fastalib = importlib.import_module('gx3nwe_assignment1')
       proteins = fastalib.load_fasta(fullpath)
    retval = ""
    if (function == 'num_with_motif'):
        num_proteins = len(fastalib.find_protein_with_motif(proteins, arg))
        retval = num_proteins
    elif (function == 'num_amino_acids'):
        aminoacidcounts = 0
        for p in proteins:
            aminoacidcounts = aminoacidcounts + sum(p.aminoacidcounts.values())
        retval = aminoacidcounts
    else:
        retval = 'bad function'
    num_proteins = len(proteins)
    result = json.dumps({'retval': retval, 'num_proteins': num_proteins})
    return result

@app.route('/mongo/names', methods=['POST'])
def mongo_names():
    jsondata = request.json
    iteration =  int(jsondata['iteration'])
    myclient = pymongo.MongoClient('mongodb://localhost:27017/')
    mydb = myclient['PYTHONCLASS2020']
    mycol = mydb['main']
    myquery = { 'iteration': iteration }
    items = mycol.find(myquery)
    names = []
    for item in items:
       names.append(item['name'])
    result = json.dumps(names)
    return result

@app.route('/mongo/clear')
def mongo_clear():
    iteration = request.args.get('iteration')
    myclient = pymongo.MongoClient('mongodb://localhost:27017/')
    mydb = myclient['PYTHONCLASS2020']
    mycol = mydb['gx3nwe']
    mycol.drop()
    result = 'ok'
    return result

@app.route('/mongo/create_user', methods=['POST'])
def mongo_create_user():
    jsondata = request.json
    username = jsondata['username']
    email = jsondata['email']
    password = jsondata['password']
    name = jsondata['name']
    myclient = pymongo.MongoClient('mongodb://localhost:27017/')
    mydb = myclient['PYTHONCLASS2020']
    mycol = mydb['gx3nwe']
    mydict = { 'username': username, 'email': email, 'password': password, 'name': name }
    x = mycol.insert_one(mydict)
    result = "ok"
    return result

@app.route('/mongo/auth/names', methods=['POST'])
def mongo_names_auth():
    jsondata = request.json
    username = jsondata['username']
    password = jsondata['password']
    iteration = jsondata['iteration']
    myclient = pymongo.MongoClient('mongodb://localhost:27017/')
    mydb = myclient['PYTHONCLASS2020']
    mycol = mydb['gx3nwe']
    myquery = { 'username': username}
    count = mycol.count_documents(myquery)
    mydoc = mycol.find(myquery)
    names = []
    if (count == 0):
        result = json.dumps({'list_of_names': names,'status': 'bad username'})
    else:
        user = mydoc.next()
        if (user['password'] != password):
            result = json.dumps({'list_of_names': names, 'status': 'bad password'})
        else:
            mycol = mydb['main']
            myquery = { 'iteration': iteration }
            items = mycol.find(myquery)
            for item in items:
                names.append(item['name'])
            result = json.dumps({'list_of_names': names, 'status': 'ok'})
    return result

@app.route('/svm', methods=['POST'])
def svm():
    jsondata = request.json
    c = jsondata['C']
    gamma = jsondata['gamma']
    data = jsondata['data']
    iris = load_iris()
    target_names = iris.target_names
    model = SVC(C=c, gamma=gamma, probability=True) 
    model.fit(iris.data,iris.target)
    predict_indexes = model.predict(data)
    predicts = model.predict_proba(data)
    values = []
    index = 0
    for predict in predicts:
        species = target_names[predict_indexes[index]]
        probability = predict[predict_indexes[index]]
        mydict = { 'species': species, 'probability': probability }
        values.append(mydict)
        index += 1
    result = json.dumps(values)
    return result

if __name__ == '__main__':
    app.run(port=5010)